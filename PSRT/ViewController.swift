//
//  ViewController.swift
//  PSRT
//
//  Created by Reena Singh on 2/18/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//
/* App: PaperScissorRock Game
** Description: The game is played between player and computer. Player plays by clicking the button of 'Paper' or 'Scissor' or 'Rock'. On click of any of this button on this ViewController it will present the modally ResultViewController with the result already computed in the background. The result will be displayed who wins. Also ResultViewController will display what was played by Player and computer. Computer plays randomly. The logic is designed in RPS.swift logic. Who wins is determined by RPSMatch.swift logic.
*/

import UIKit


// ViewController 1:
class ViewController: UIViewController {

   //Rock: Present the view controller completely in code using presentViewController(_:animated:completion:).
    
    @IBAction func flipToResultforRock(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "Rock", sender: nil)
        let resultViewController = self.storyboard!.instantiateViewController(withIdentifier: "rc") as! ResultViewC
        self.present(resultViewController, animated: true, completion: nil)
    }

    //Paper: Present the view controller with a manually triggered segue, using performSegueWithIdentifier(_:sender:)
    @IBAction func flipToResultforPaper(_ sender: UIButton) {
        self.performSegue(withIdentifier: "Paper", sender: nil)
    }
   
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Scissors: Present the view controller with automatically triggered segue, attached directly to the button.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // If player clicks 'Paper' button, and opponentMove(computer) is as follows:
        if segue.identifier == "Paper" {
            
            //Displays result who wins
            let resultViewController = segue.destination as! ResultViewC
            let opponentMove = RPS()
            let whoWins = RPSMatch(RPS: "Paper", RPS: opponentMove.description)
             resultViewController.player = "PlayerMove:= Paper"
             resultViewController.computerMove = "ComputerMove:=\(opponentMove.description)"
             resultViewController.value = "WhoWins:=\(whoWins.description)"
            
            //Displays different image depending on who wins : Scissor can cut paper: hence Paper.Loss
            if(opponentMove.description == "Scissors"){
                let image = UIImage(named:"20150220_184058.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
            // Rock can hold paper. hence Rock.Wins
            if(opponentMove.description == "Rock"){
                let image = UIImage(named:"20150220_184036.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
            //Displays super orange image to tell both party: player and opponent ..withdraws (Tie) ->(nobody win or lose)
            if(opponentMove.description == "Paper"){
                let image = UIImage(named:"20150223_120152.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }

        }
        
         // If player clicks 'Scissor' button, and opponentMove(computer) is as follows:
        if segue.identifier == "Scissor" {
            
            //Displays result who wins
            let resultViewController = segue.destination as! ResultViewC
            let opponentMove = RPS()
            let whoWins = RPSMatch(RPS: "Scissors", RPS: opponentMove.description)
            resultViewController.player = "PlayerMove:= Scissors"
            resultViewController.computerMove = "ComputerMove:=\(opponentMove.description)"
            resultViewController.value = "WhoWins:=\(whoWins.description)"
            
            //Displays different image depending on who wins: Paper.Loss because, scissor can cut paper
            if(opponentMove.description == "Paper"){
                let image = UIImage(named:"20150220_184058.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
            //Rock.wins over scissor because scissor cant cut rock.
            if(opponentMove.description == "Rock"){
                let image = UIImage(named:"20150220_183954.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
            //Tie
            if(opponentMove.description == "Scissors"){
                let image = UIImage(named:"20150223_120152.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
        }
         // If player clicks 'Rock' button, and opponentMove(computer) is as follows:
        if segue.identifier == "Rock" {
            
            //Displays result who wins
            let resultViewController = segue.destination as! ResultViewC
            let opponentMove = RPS()
            let whoWins = RPSMatch(RPS: "Rock", RPS: opponentMove.description)
             resultViewController.player = "PlayerMove:= Rock"
            resultViewController.computerMove = "ComputerMove:=\(opponentMove.description)"
            resultViewController.value = "WhoWins:=\(whoWins.description)"
            
            //Displays different image depending on who wins : Paper.Loss because Rock can hold paper underneath
            if(opponentMove.description == "Paper"){
                let image = UIImage(named:"20150220_184036.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            // Rock.wins because scissors cant cut rock
            if(opponentMove.description == "Scissors"){
                let image = UIImage(named:"20150220_183954.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
            //Tie
            if(opponentMove.description == "Rock"){
                let image = UIImage(named:"20150223_120152.jpg")
                let imagePaper = segue.destination as! ResultViewC
                imagePaper.img = image
            }
            
        }
    }

}

