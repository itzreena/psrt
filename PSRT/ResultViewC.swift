//
//  ResultViewController.swift
//  PSRT
//
//  Created by Reena Singh on 2/18/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//
/*
ResultViewController: This controller will get all the computed values coming from previous VC, and then it will populate the label with results to display on this VC. This controller has two button, one button on the navigation bar (A) which will display the last result value of winner and the other button is just below result (save button). (B) Click save button to save result.
** How To Play::=

Steps: 1)   Play Click (Rock | Paper | Scissor)
       2)   Click (save) button
       3)   Click (Back), 
       4)   play Click (Rock | Paper | Scissor)
       5)   Click (Game winner History)
       6)   Click (saveHistory)
       7)   Click (last history)

        See results of two play and compare. 
**

*/

import UIKit

// ViewController 2: ResultViewController
class ResultViewC: UIViewController{
    
    
    
    //Outlet and Action defined to be used by this controller
    
    @IBOutlet weak var players: UILabel!
    
  
    
    @IBAction func historyOfR(_ sender: Any) {
        self.performSegue(withIdentifier: "history", sender: nil)
    }
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var computer: UILabel!
    
    @IBOutlet weak var result: UILabel!
    
    //Properties & Global variables
    
    @IBAction func PlayAgain(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    var value = ""
    var player = ""
    var computerMove = ""
    var img: UIImage!
    var hist:String = ""
    var  historyOfGamePlayed = [String]()
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        if segue.identifier == "history"{
            
            let historyViewController = segue.destination as! HistoryViewController
            historyViewController.hist = result.text!
            historyOfGamePlayed.append(historyViewController.hist)
            UserDefaults.standard.set(historyViewController.hist, forKey: "array")
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"History", style:.plain, target:self, action:#selector(ResultViewC.historyNavigation))
    }
    
    func historyNavigation(){
        
        //nextViewController is the next controllers in sequence
        let nextViewController = (self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController"))! as UIViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // Saves the result using NSUserDefault, the result is stored using key 'results', later this key is retrieved to display value
    // The result is persistent: Re-run program and click History Button, then click last history button to retrieve result
   
    
    @IBAction func SaveResult(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(self.result.text, forKey: "results")
        defaults.synchronize()
    }
    
    //View will appear depends on result when button is clicked on ViewController and who wins
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //UIImageView will take UIImage
        let myImageView = UIImageView(image: img)
        
        //frame is created to display image in SubView
        myImageView.frame = view.frame
        myImageView.frame = CGRect(x: 100, y: 225, width: 200, height: 200)
        
        //Subview is added to Actual view to display the image according to frame set.
        view.addSubview(myImageView)
        // image.image = img
        
        
        //Displays what player clicked on screen
        players.text = "\(player)"
        
        //Displays what computer move randomly
        computer.text = "\(computerMove)"
        
        //Displays who Wins
        result.text = "\(value)"
        
        
    }
    
}
