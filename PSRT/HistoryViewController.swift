//
//  HistoryViewController.swift
//  RPSGame
//
//  Created by Reena Singh on 3/12/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    var historyOfWinner : Array<AnyObject> = []

    var hist : String = ""
 
    var value : String = ""
   
  //  var  historyOfGamePlayed = [String]()
    
  //  var history = [NSManagedObject]()
    var newhist : String = ""

    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        
        if segue.identifier == "history"{
            
            let rvc = segue.source as! ResultViewController
        
                hist = rvc.result.text!
                historyOfWinner.append(hist as AnyObject)
                 print("ree\(historyOfWinner)")  //yes
                 UserDefaults.standard.set(historyOfWinner, forKey: "array")

        }
    }
    
  
    
    @IBAction func cancel(segue:UIStoryboardSegue){
        
    }
    
    @IBAction func done(segue:UIStoryboardSegue) {
        let runningHistory = segue.source as! AddHistoryViewController
        
        print("reena\(runningHistory.hist)") // yes
        newhist = runningHistory.hist
     
        historyOfWinner.append(hist as AnyObject)
        print("ree\(historyOfWinner)")  //yes
        UserDefaults.standard.set(historyOfWinner, forKey: "array")
        
        
    }
    
    override func viewDidLoad() {
    
    super.viewDidLoad()
        
      
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone(abbreviation: "BST")!
        let localDate = dateFormatter.string(from: date)
        
    print(localDate)
    print("moon\(hist)")
        
    let histVars: [String: String] = [hist:localDate]
        
        print("sun\(histVars)")
        print("hist\(hist)")
       
       historyOfWinner.append(histVars as AnyObject)    
        UserDefaults.standard.set(hist /*historyOfWinner*/, forKey: "array")
    }
    

    
//    override func viewWillAppear(animated: Bool) {
//    
//    var saveHistory: NSUserDefaults = NSUserDefaults.standardUserDefaults()
//    var theText = saveHistory.stringForKey("array")!
//    value = theText
//    
//    
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
    
   // var ObjStr : NSString = value as NSString
    UserDefaults.standard.set(historyOfWinner, forKey: "array")
    }
    
    
    // MARK: - Table View Data Source
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return historyOfWinner.count
        
    }
    
    
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    
    // Get a reuable cell from the table view
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ResultTableView
    
    
    
    // Configure the cell
    
    let obj: AnyObject = historyOfWinner[indexPath.row]
   

    cell.resultLabel!.text = (obj as! String)
    
    
    
    cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
    
    
    
   // cell.detailTextLabel
    
    
    
    // Return the cell
    
    return cell
    
    }
    
    
    
    private func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
    
    print(historyOfWinner[indexPath.row]);
        
        print("\n")
    }
    
    

}
