//
//  RPS.swift
//  PSRT
//
//  Created by Reena Singh on 2/19/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//
/**********************************************************************************************************************/
// Author: Reena Singh
/* The RPS enum:
The RPS enum should have three cases: Rock, Paper, and Scissors. It should have an init method that randomly creates an instance with the value RPS.Rock, RPS.Paper, and RPS.Scissors with equal probability.
It should also conform to the Printable protocol by including a String property named description.
**********************************************************************************************************************/
import Foundation
import UIKit

//RPS enum confrom to Printable protocol
enum RPS: CustomStringConvertible {
    
    // Three Cases
    case rock, paper, scissors
    
    //init method
    init() {
        
        var arrayInt = ["Rock","Paper","Paper","Scissors","Rock","Paper","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors","Rock","Paper","Scissors"]
        
        //Counts length of array
        let count = arrayInt.count
        
        //that randomly creates instance with equal probability of Rock, Paper, Scissors
        let r : Int = Int(arc4random())%(count - 1)
        let w = arrayInt[r]
        arrayInt.remove(at: r)
        
        
        if(w == "Rock"){
            self = .rock
        }
        else if (w == "Paper"){
            self = .paper
        }
        else if (w == "Scissors"){
            self = .scissors
        }
        else {
            self = .paper
        }
        
    }
    
    // the description property from the Printable protocol
    var description: String {
        switch (self) {
        case .rock:
            return "Rock"
        case .paper:
            return "Paper"
        case .scissors:
            return "Scissors"
        }
    }
}




