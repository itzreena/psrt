//
//  OutComeOne.swift
//  PSRT
//
//  Created by Reena Singh on 2/20/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//

import Foundation
import UIKit

class OutComeOne : UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "20150220_183954.jpg")!)
    }

}