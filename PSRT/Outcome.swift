//
//  Outcome.swift
//  PSRT
//
//  Created by Reena Singh on 2/19/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//

import Foundation
import UIKit

/********************************************************************************************************/
//The Outcome enum:
//This is a simple enum with three cases: Win, Loss, and Tie. It should also conform the Printable protocol.
/*********************************************************************************************************/
enum Outcome: Printable {
    
    case Win, Loss, Tie
    
    init(RPS result: String){
        
        var rString = result
        
        if(rString == "Rock"){
            self = .Win
        }
            
        else if ( rString == "Paper"){
            self = .Loss
        }
            
        else if( rString == "Scissors"){
            self = .Tie
        }
        else {
            self = .Tie
        }
    }
    
    var description: String {
        switch (self) {
        case .Win:
            return "Win"
        case .Loss:
            return "Loss"
        case .Tie:
            return "Tie"
        }
    }
}

