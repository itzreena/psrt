//
//  RPSMatch.swift
//  PSRT
//
//  Created by Reena Singh on 2/19/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//
/********************************************************************************************************/
/*The RPSMatch:
This struct represents a single Rock Paper Scissors match.
It should have the following properties:
playerMove: RPS
opponentMove: RPS
outcome: Outcome
description: string
The outcome and description properties should be calculated properties. T
he description should be in the form “Rock vs. Paper. Loss.” or “Scissors vs. Scissors. Tie.”
********************************************************************************************************/
import Foundation
import UIKit
struct RPSMatch {
    
    
    var possibility = " "
    
    init(RPS playerMove: String, RPS opponentMove: String){
        
        // Calculating Outcome and description between playerMove and opponentMove
        
        if(playerMove == "Rock") && (opponentMove == "Rock"){
        
            possibility = "Tie"
            
        }
        if(playerMove == "Scissors") && (opponentMove == "Scissors"){
            
            possibility = "Tie"
            
        }
        if(playerMove == "Paper") && (opponentMove == "Paper"){
            
            possibility = "Tie"
            
        }
        
        if(playerMove == "Rock") && (opponentMove == "Scissors"){
            
            possibility = "Rock"
            
        }
        if(playerMove == "Paper") && (opponentMove == "Rock"){
            
            possibility = "Paper"
            
        }
        if(playerMove == "Scissors") && (opponentMove == "Paper"){
            
            possibility = "Paper"
            
        }
        
        if(playerMove == "Scissors") && (opponentMove == "Rock"){
            
            possibility = "Rock"
            
        }
        
        if(playerMove == "Rock") && (opponentMove == "Paper"){
            
            possibility = "Paper"
            
        }

        if(playerMove == "Paper") && (opponentMove == "Scissors"){
            
            possibility = "Paper"
            
        }
        
        
    }
    
    
    var description: String {
        switch possibility {
            
        case "Rock":
            return "Rock.Win"
        case "Paper":
            return "Paper.Loss"
        case "Tie":
            return "Tie"
        default:
            return " ";
        }
    }
    
    
}

