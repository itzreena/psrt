//
//  ResultViewController.swift
//  PSRT
//
//  Created by Reena Singh on 2/18/15.
//  Copyright (c) 2015 ccsf. All rights reserved.
//

import UIKit

// ViewController 2: ResultViewController
class ResultViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // PlayAgain will dismiss ResultViewController and goes back to ViewController 1
    @IBAction func PlayAgain(_ sender: AnyObject) {
        
        dismiss(animated: true, completion: nil)
        
    }

    // Outlet : Properties defined
    @IBOutlet weak var players: UILabel!
    @IBOutlet weak var result: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var computer: UILabel!

    //Initialize global variable
    var value = ""
    var player = ""
    var computerMove = ""
    var img: UIImage!

 
    //View will appear depends on result when button is clicked on ViewController and who wins
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //UIImageView will take UIImage
        let myImageView = UIImageView(image: img)
        
        //frame is created to display image in SubView
        myImageView.frame = view.frame
        myImageView.frame = CGRect(x: 100, y: 100, width: 175, height: 175)
        
        //Subview is added to Actual view to display the image according to frame set.
        view.addSubview(myImageView)
        image.image = img
        
        //Displays what player clicked on screen
        players.text = "\(player)"
        
        //Displays what computer move randomly
        computer.text = "\(computerMove)"
        
        //Displays who Wins
        result.text = "\(value)"
    }
    


}
